This repository is for users of the uMMORPG and Unity communities to better
understand the Add-On system for uMMORPG and other basic Unity concepts.

This repository is licensed under MIT License, if you do no understand how this
license works, then kindly visit https://tldrlegal.com/license/mit-license